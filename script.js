'use strict';

const albums = document.querySelector('.albums');
const photos = document.querySelector('.photos');
const URL_ALBUM = 'https://jsonplaceholder.typicode.com/albums';
const URL_PHOTOS = 'https://jsonplaceholder.typicode.com/photos?albumId={{id}}';

getAlbumList();

albums.addEventListener('click', onAlbumsClick);

function onAlbumsClick(e) {
    e.preventDefault();
    const linkId = e.target.dataset.id;
    
    if (linkId) {
        getPhotosById(linkId);
    }
}

function getPhotosById(id) {
    fetch(URL_PHOTOS.replace('{{id}}', id))
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        })
        .then(function (data) {
            renderPhotos(data);
        })
        .catch((e) => {
            showError(e);
        });
}

function renderPhotos(data) {
    const html = data.map(generateInfoHtml).join('');
    photos.innerHTML = html;
}

function generateInfoHtml(data) {

    return `
        <div class="item">
            <img alt="${data.id}" src="${data.url}" />
        </div>
    `;
}

function getAlbumList() {
    return fetch(URL_ALBUM)
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        })
        .then(function (data) {
            let initialId = data?.[0]?.id;
            renderAlbum(data)
            if (initialId) {
                getPhotosById(initialId)
            }
            
        })
        .catch((e) => {
            showError(e);
        });
}

function renderAlbum(data) {
    const html = data.map(generateAlbumItemHtml).join('');

    albums.insertAdjacentHTML('beforeend', html);
}

function generateAlbumItemHtml(data) {

    return `
        <a data-id="${data.id}" href="#">${data.id}. ${data.title}</a>
    `;
}

function showError(e) {
    alert(e.message);
}